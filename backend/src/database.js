const mongoose = require('mongoose');
const URI = process.env.MONGODB_URI || 'mongodb://localhost/dbtest';

mongoose
  .connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then((db) => console.log('DB is connected'))
  .catch((err) => console.log(err));
