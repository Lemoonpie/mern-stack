const noteCtrl = {};
const Note = require('../models/Note');

noteCtrl.getNotes = async (req, res) => {
  const notes = await Note.find();
  res.json(notes);
};

noteCtrl.createNote = async (req, res) => {
  const note = new Note(req.body);
  await note.save();
  res.json({ message: 'Note Saved' });
};

noteCtrl.getNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  res.json(note);
};

noteCtrl.updateNote = async (req, res) => {
  await Note.findOneAndUpdate(req.params.id, req.body);
  res.json({ message: 'Note Updated' });
};

noteCtrl.deleteNote = async (req, res) => {
  await Note.findByIdAndDelete(req.params.id);
  res.json({ message: 'Note Deleted' });
};

module.exports = noteCtrl;
