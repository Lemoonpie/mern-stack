const userCtrl = {};
const User = require('../models/User');

userCtrl.getUsers = async (req, res) => {
  const users = await User.find();
  res.json(users);
};

userCtrl.createUser = async (req, res) => {
  const user = new User(req.body);
  await user.save();
  res.json({ mesagge: 'User saved' });
};

userCtrl.getUser = async (req, res) => {
  const user = await User.findById(req.params.id);
  res.json(user);
};

userCtrl.updateUser = async (req, res) => {
  await User.findByIdAndUpdate(req.params.id, req.body);
  res.json({ mesagge: 'User update' });
};

userCtrl.deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.params.id);
  res.json({ mesagge: 'User deleted' });
};

module.exports = userCtrl;
