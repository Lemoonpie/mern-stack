const { Router } = require('express');
const router = Router();
const {
  getUsers,
  getUser,
  createUser,
  deleteUser,
  updateUser,
} = require('../controllers/users.controller');

router.route('/').get(getUsers);
router.route('/').post(createUser);

router.route('/:id').get(getUser);
router.route('/:id').post(updateUser);
router.route('/:id').delete(deleteUser);

module.exports = router;
